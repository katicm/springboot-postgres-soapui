
INSERT INTO "proizvodjac"("id", "naziv", "adresa", "kontakt")
VALUES(nextval('proizvodjac_seq'), 'AD Imlek', 'Industrijsko naselje bb, Padinska skela, Beograd', '+381 11 30 50 505');
INSERT INTO "proizvodjac"("id", "naziv", "adresa", "kontakt")
VALUES(nextval('proizvodjac_seq'), 'Henkel Srbija', 'Bulevar oslobodenja 383, 11040 Beograd, Srbija', '+381 11 20 72 200');
INSERT INTO "proizvodjac"("id", "naziv", "adresa", "kontakt")
VALUES(nextval('proizvodjac_seq'), 'Fruit D.O.O.', 'Justina Popovica 3, 11080 Zemun, Beograd', '+381 11 3143 171');
INSERT INTO "proizvodjac"("id", "naziv", "adresa", "kontakt")
VALUES(nextval('proizvodjac_seq'), 'CENTROPROIZVOD', 'DOBANOVACKI PUT B.B. 11271, SURCIN', '+381 11 3773 600');

INSERT INTO "proizvodjac"("id", "naziv", "adresa", "kontakt")
VALUES(-100, 'Naziv Test', 'Adresa Test', 'Kontakt Test');

INSERT INTO "proizvod"("id", "naziv", "proizvodjac")
VALUES(nextval('proizvod_seq'), 'Pavlaka 1kg', 1);
INSERT INTO "proizvod"("id", "naziv", "proizvodjac")
VALUES(nextval('proizvod_seq'), 'Jogurt 1kg', 1);
INSERT INTO "proizvod"("id", "naziv", "proizvodjac")
VALUES(nextval('proizvod_seq'), 'Pivo 0.5', 2);
INSERT INTO "proizvod"("id", "naziv", "proizvodjac")
VALUES(nextval('proizvod_seq'), 'Mleko', 1);
INSERT INTO "proizvod"("id", "naziv", "proizvodjac")
VALUES(nextval('proizvod_seq'), 'Sok Jabuka 1l', 3);
INSERT INTO "proizvod"("id", "naziv", "proizvodjac")
VALUES(nextval('proizvod_seq'), 'Šlag pena', 4);

INSERT INTO "proizvod"("id", "naziv", "proizvodjac")
VALUES(-100, 'Naziv Test', 1);

INSERT INTO "racun"("id", "datum", "nacin_placanja")
VALUES(nextval('racun_seq'), '2017-03-14', 'bank transfer');
INSERT INTO "racun"("id", "datum", "nacin_placanja")
VALUES(nextval('racun_seq'), '2017-03-11', 'cash');
INSERT INTO "racun"("id", "datum", "nacin_placanja")
VALUES(nextval('racun_seq'), '2017-03-16', 'card');
INSERT INTO "racun"("id", "datum", "nacin_placanja")
VALUES(nextval('racun_seq'), '2017-03-18', 'cash');
INSERT INTO "racun"("id", "datum", "nacin_placanja")
VALUES(nextval('racun_seq'), '2017-03-19', 'card');

INSERT INTO "racun"("id", "datum", "nacin_placanja")
VALUES(-100, '1900-01-01', 'Nacin placanja Test');

INSERT INTO "stavka_racuna"("id", "redni_broj", "kolicina","jedinica_mere","cena","racun","proizvod")
VALUES(nextval('stavka_racuna_seq'), '22404', 100, 'kilogram', '15000', 1, 1);
INSERT INTO "stavka_racuna"("id", "redni_broj", "kolicina","jedinica_mere","cena","racun","proizvod")
VALUES(nextval('stavka_racuna_seq'), '64532', 1000, 'kilogram', '30000', 2, 2);
INSERT INTO "stavka_racuna"("id", "redni_broj", "kolicina","jedinica_mere","cena","racun","proizvod")
VALUES(nextval('stavka_racuna_seq'), '32321', 200, 'litra', '9000', 3, 3);
INSERT INTO "stavka_racuna"("id", "redni_broj", "kolicina","jedinica_mere","cena","racun","proizvod")
VALUES(nextval('stavka_racuna_seq'), '23421', 350, 'litra', '19000', 4, 5);
INSERT INTO "stavka_racuna"("id", "redni_broj", "kolicina","jedinica_mere","cena","racun","proizvod")
VALUES(nextval('stavka_racuna_seq'), '23224', 350, 'litra', '22000', 5, 4);

INSERT INTO "stavka_racuna"("id", "redni_broj", "kolicina","jedinica_mere","cena","racun","proizvod")
VALUES(-100, '54242', 350, 'litra Test', '22000', 1, 1);
