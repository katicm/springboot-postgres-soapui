package rva.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the stavka_racuna database table.
 *
 */
@Entity
@Table(name="stavka_racuna")
@NamedQuery(name="StavkaRacuna.findAll", query="SELECT s FROM StavkaRacuna s")
public class StavkaRacuna implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="STAVKA_RACUNA_ID_GENERATOR", sequenceName="STAVKA_RACUNA_SEQ", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="STAVKA_RACUNA_ID_GENERATOR")
	private Integer id;

	@Column(name="redni_broj")
	private Integer redni_broj;

	private BigDecimal kolicina;

	@Column(name="jedinica_mere")
	private String jedinica_mere;
	
	private BigDecimal cena;

	//bi-directional many-to-one association to Racun
	@ManyToOne
	@JoinColumn(name="racun")
	private Racun racun;

	//bi-directional many-to-one association to Proizvod
	@ManyToOne
	@JoinColumn(name="proizvod")
	private Proizvod proizvod;

	public StavkaRacuna() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BigDecimal getCena() {
		return this.cena;
	}

	public void setCena(BigDecimal cena) {
		this.cena = cena;
	}

	public String getJedinica_mere() {
		return this.jedinica_mere;
	}

	public void setJedinica_mere(String jedinica_mere) {
		this.jedinica_mere = jedinica_mere;
	}

	public BigDecimal getKolicina() {
		return this.kolicina;
	}

	public void setKolicina(BigDecimal kolicina) {
		this.kolicina = kolicina;
	}

	public Integer getRedniBroj() {
		return this.redni_broj;
	}

	public void setRedniBroj(Integer redni_broj) {
		this.redni_broj = redni_broj;
	}

	public Racun getRacun() {
		return this.racun;
	}

	public void setRacun(Racun racun) {
		this.racun = racun;
	}

	public Proizvod getProizvod() {
		return this.proizvod;
	}

	public void setProizvod(Proizvod proizvod) {
		this.proizvod = proizvod;
	}

}
