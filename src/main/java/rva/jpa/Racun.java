package rva.jpa;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;
import java.util.List;


/**
 * The persistent class for the racun database table.
 *
 */
@Entity
@NamedQuery(name="Racun.findAll", query="SELECT a FROM Racun a")
public class Racun implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="RACUN_ID_GENERATOR", sequenceName="RACUN_SEQ", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="RACUN_ID_GENERATOR")
	private Integer id;

	@Temporal(TemporalType.DATE)
	private Date datum;

	private String nacin_placanja;

	//bi-directional many-to-one association to StavkaProizvod
	@OneToMany(mappedBy="racun")
	@JsonIgnore
	private List<StavkaRacuna> stavkaRacuna;

	public Racun() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDatum() {
		return this.datum;
	}

	public void setDatum(Date datum) {
		this.datum = datum;
	}

	public String getNacin_placanja() {
		return this.nacin_placanja;
	}

	public void setNacin_placanja(String nacin_placanja) {
		this.nacin_placanja = nacin_placanja;
	}

	public List<StavkaRacuna> getStavkaRacuna() {
		return this.stavkaRacuna;
	}

	public void setStavkaProizvods(List<StavkaRacuna> stavkaRacuna) {
		this.stavkaRacuna = stavkaRacuna;
	}

	public StavkaRacuna addStavkaProizvod(StavkaRacuna stavkaRacuna) {
		getStavkaRacuna().add(stavkaRacuna);
		stavkaRacuna.setRacun(this);

		return stavkaRacuna;
	}

	public StavkaRacuna removeStavkaProizvod(StavkaRacuna stavkaRacuna) {
		getStavkaRacuna().remove(stavkaRacuna);
		stavkaRacuna.setRacun(null);

		return stavkaRacuna;
	}

}
