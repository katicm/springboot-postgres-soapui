package rva.ctrls;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import rva.reps.RacunRepository;
import rva.jpa.*;

@Api(tags = {"Racun CRUD operacije"})
@RestController
public class RacunRestController {

	@Autowired
	private RacunRepository racunRepository;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@ApiOperation(value = "VraÄ‡a kolekciju svih racuna iz baze podataka")
	@GetMapping("racun")
	public Collection<Racun> getRacuni() {
		return racunRepository.findAll();
	}

	@ApiOperation(value = "VraÄ‡a racun iz baze podataka Ä�iji je id vrednost prosleÄ‘ena kao path varijabla")
	@GetMapping("racun/{id}")
	public Racun getRacun(@PathVariable("id") Integer id) {
		return racunRepository.getOne(id);
	}

	@ApiOperation(value = "BriÅ¡e racun iz baze podataka Ä�iji je id vrednost prosleÄ‘ena kao path varijabla")
	@CrossOrigin
	@DeleteMapping("racun/{id}")
	public ResponseEntity<Racun> deleteRacun(@PathVariable("id") Integer id) {
		if(!racunRepository.existsById(id))
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		jdbcTemplate.execute("delete from stavka_racuna where racun="+id);
		racunRepository.deleteById(id);
		if(id == -100)
			jdbcTemplate.execute(" INSERT INTO \"racun\"(\"id\", \"datum\", \"nacin_placanja\") VALUES (-100, '2018-03-03', 'placanja test')" );
		return new ResponseEntity<>(HttpStatus.OK);
	}

	// insert
	@CrossOrigin
	@PostMapping("racun")
	@ApiOperation(value = "Upisuje racun u bazu podataka")
	public ResponseEntity<Racun> insertRacun(@RequestBody Racun racun) {
		if(!racunRepository.existsById(racun.getId())) {
			racunRepository.save(racun);
			return new ResponseEntity<>(HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.CONFLICT);
	}

	// update
	@CrossOrigin
	@PutMapping("racun")
	@ApiOperation(value = "Modifikuje postojeÄ‡i racun u bazi podataka")
	public ResponseEntity<Racun> updateRacun(@RequestBody Racun racun) {
		if(!racunRepository.existsById(racun.getId()))
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		racunRepository.save(racun);
		return new ResponseEntity<>(HttpStatus.OK);
	}

}
