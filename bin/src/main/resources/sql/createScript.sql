drop table if exists racun cascade;
drop table if exists proizvodjac cascade;
drop table if exists proizvod cascade;
drop table if exists stavka_racuna cascade;
drop sequence if exists racun_seq;
drop sequence if exists proizvodjac_seq;
drop sequence if exists proizvod_seq;
drop sequence if exists stavka_racuna_seq;

create table racun(
	id integer not null,
    datum date not null,
    nacin_placanja varchar(200)
);

create table proizvodjac(
	id integer not null,
    naziv varchar(50) not null,
    adresa varchar(200) not null,
    kontakt varchar(100) not null
);

create table proizvod(
	id integer not null,
    naziv varchar(50) not null,
    proizvodjac integer not null
);

create table stavka_racuna(
	id integer not null,
   	redni_broj integer not null,
    kolicina numeric not null,
    jedinica_mere varchar(50) not null,
    cena numeric not null,
    racun integer not null,
    proizvod integer not null
);

alter table racun add constraint PK_racun primary key (id);
alter table proizvodjac add constraint PK_proizvodjac primary key (id);
alter table proizvod add constraint PK_proizvod primary key (id);
alter table stavka_racuna add constraint PK_stavka_racuna primary key (id);

alter table proizvod add constraint FK_proizvod_proizvodjac
foreign key (proizvodjac) references proizvodjac (id);

alter table stavka_racuna add constraint FK_stavka_racuna_proizvod
foreign key (proizvod) references proizvod (id);
alter table stavka_racuna add constraint FK_stavka_racuna_racun
foreign key (racun) references racun (id);

create index IDXFK_proizvod_proizvodjac on proizvod (proizvodjac);
create index IDXFK_stavka_racuna_proizvod on stavka_racuna (proizvod);
create index IDXFK_stavka_racuna_racun on stavka_racuna (racun);

create sequence racun_seq increment 1;
create sequence proizvodjac_seq increment 1;
create sequence proizvod_seq increment 1;
create sequence stavka_racuna_seq increment 1;
